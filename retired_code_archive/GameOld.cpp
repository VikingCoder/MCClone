//
// Created by Jonas on 7/14/18.
//


#include "GameOld.h"
#include "../src/game/Input.h"
#include "../src/rendering/Window.h"
#include "../src/util/Time.h"
#include "../src/util/ModelLoader.h"
#include "../src/rendering/lighting/DirectionalLight.h"
#include "../src/rendering/lighting/SpotLight.h"
#include "../src/game/Generators/DungeonGenerator.h"
#include "../src/game/Generators/OverWorldGenerator.h"
#include "../src/rendering/scene-tree/Scene.h"
#include "../src/rendering/scene-tree/entities/Actor.h"
#include "../src/math/types/Vec2.h"


glm::vec3 ambientLight;

Mesh mesh;
std::vector<Mesh> dungeon;
std::vector<Mesh> overworld;
Scene scene;

DirectionalLight d;
std::vector<PointLight> p;
SpotLight flashLight;
//AbstractCamera* camera;

int GameOld::init() {
    AbstractCamera* camera = new FreeCamera(glm::vec3(10.0f, 10.0f, 30.0f));
    Input::setCamera(camera);

    Vec2<int> blu(1, 2);
    Vec2<int> bli(2, 1);
    std::cout << blu * bli << std::endl;

    DungeonGenerator dg(79, 24);
    dg.generate(50);
    dg.print();
    dungeon = dg.generateMesh(50);
    overworld = OverWorldGenerator::generateMesh(2, 2);

    ambientLight = glm::vec3(0.4f, 0.4f, 0.4f);

    shader = PhongShader("rendering/shading/shader_code/phongVertex.glsl", "rendering/shading/shader_code/phongFragment.glsl");

    mesh = ModelLoader::loadMesh("assets/models/FPS-Test-Room.obj");
    Texture t0 = Texture("assets/textures/prototype-textures/PNG/Purple/texture_04.png");
    Texture t1 = Texture("assets/textures/prototype-textures/PNG/Dark/texture_13.png");

    mesh.setMaterial(Material(glm::vec3(0.4f, 0.4f, 0.4f)));
    dungeon[0].setMaterial(Material(t0, glm::vec3(.5f, .5f, .5f)));
    dungeon[1].setMaterial(Material(t1, glm::vec3(1.0f, 1.0f, 1.0f)));

    overworld[0].setMaterial(Material(glm::vec3(0.0f, .66f, .22f)));

    mesh.init();
    dungeon[0].init();
    dungeon[1].init();
    overworld[0].init();

    BaseLight pointBase;
    pointBase.setColor(glm::vec3(1, 1, 1));
    pointBase.setIntensity(0.5f);

    p.emplace_back(PointLight());
    p[0].setBaseLight(pointBase);

    pointBase.setColor(glm::vec3(1, 1, 1));
    pointBase.setIntensity(1.0f);

    flashLight.getPointLight().setBaseLight(pointBase);

    auto *addedToRoot1 = new Entity("addedToRoot1");
    auto *addedToRoot2 = new Entity("addedToRoot2");
    Entity *bla = new Actor();
    auto *addedToAddedToRoot11 = new Entity("addedToAddedToRoot11");;
    auto *addedToAddedToRoot12 = new Entity("addedToAddedToRoot12");;
    auto *addedToAddedToRoot21 = new Entity("addedToAddedToRoot21");;

    scene.addEntityToRoot(addedToRoot1);
    scene.addEntityToRoot(addedToRoot2);
    scene.addEntityToRoot(bla);
    scene.addEntity(addedToRoot1, addedToAddedToRoot11);
    scene.addEntity(addedToRoot1, addedToAddedToRoot12);
    scene.addEntity(addedToRoot2, addedToAddedToRoot21);

    scene.render();
    scene.generateGraphvizOutput();

    return 0;
}

void GameOld::input() {
    if(Input::getKeyPressed(Window::window, GLFW_KEY_W)) Input::camera->ProcessKeyboard(FORWARD, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_S)) Input::camera->ProcessKeyboard(BACKWARD, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_A)) Input::camera->ProcessKeyboard(LEFT, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_D)) Input::camera->ProcessKeyboard(RIGHT, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_LEFT_SHIFT)) Input::camera->Sprint();
    if(Input::getKeyReleased(Window::window, GLFW_KEY_LEFT_SHIFT)) Input::camera->StopSprint();
}

void GameOld::update() {

}

// TODO: create a better way to draw meshes without all the shader boilerplate
void GameOld::render() {

    for(auto & i : dungeon) {
        shader.use();

        glm::mat4 view, projection, model;

        view = Input::camera->GetViewMatrix();
        projection = glm::perspective(glm::radians(Input::camera->zoom), (float)600 / (float)400, 0.1f, 100.0f); // TODO: change aspect ratio on window resize
        model = i.model;
        //model = glm::rotate(mesh.model, glm::radians(60.0f) * (float)glfwGetTime(), glm::vec3(0.0f, 1.0f, 1.0f));

        shader.setLight(ambientLight, d);
        shader.setPointLights(p);

        flashLight.getPointLight().setPosition(Input::camera->position);
        flashLight.setDirection(Input::camera->front);

        shader.setSpotLight(flashLight);
        shader.setVec3("eyePos", Input::camera->position);

        shader.setMat4("view", view);
        shader.setMat4("projection", projection);
        shader.setMat4("model", model);

        shader.setMaterial(i.getMaterial());

        i.draw();
    }

}

void GameOld::dispose() {

}
