//
// Created by Jonas on 7/14/18.
//

#ifndef MCCLONE_GAMEOLD_H
#define MCCLONE_GAMEOLD_H


#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include "../src/include/stb_image.h"

#include "../src/game/CameraControllers/FreeCamera.h"
#include "../src/rendering/shading/PhongShader.h"

class GameOld {
public:

    PhongShader shader;

    int init();
    void input();
    void update();
    void render();
    void dispose();

};


#endif //MCCLONE_GAMEOLD_H
