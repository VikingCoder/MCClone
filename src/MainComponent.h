//
// Created by Jonas on 10/3/19.
//

#ifndef MCCLONE_MAINCOMPONENT_H
#define MCCLONE_MAINCOMPONENT_H


#include <string>
#include "game/Game.h"


class MainComponent {
public:

    constexpr static const double FRAME_CAP = 5000.0;

    MainComponent(int width, int height, const std::string &title);

    void start();
    void stop();

private:
    Game game;
    bool isRunning;

    void run();
    void render();
    void cleanUp();

};


#endif //MCCLONE_MAINCOMPONENT_H
