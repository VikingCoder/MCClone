//
// Created by Jonas on 13/7/18.
//

#ifndef MCCLONE_FREECAMERA_H
#define MCCLONE_FREECAMERA_H


#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include "AbstractCamera.h"


class FreeCamera : public AbstractCamera
{
public:
    FreeCamera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH);
    FreeCamera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);

    glm::mat4 GetViewMatrix() override;

    void Sprint() override;
    void StopSprint() override;
    void ProcessKeyboard(Camera_Movement direction, float deltaTime) override;
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true) override;
    void ProcessMouseScroll(float yoffset) override;

private:
    void updateCameraVectors();
};


#endif //MCCLONE_FREECAMERA_H
