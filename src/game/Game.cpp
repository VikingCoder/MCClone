//
// Created by Jonas on 7/1/23.
//

#include "Game.h"
#include "Input.h"
#include "../rendering/Window.h"
#include "../util/Time.h"
#include "Generators/DungeonGenerator.h"
#include "../rendering/scene-tree/entities/Actor.h"
#include "../rendering/scene-tree/entities/Camera.h"
#include "../rendering/scene-tree/entities/Light.h"
#include "CameraControllers/FPSCamera.h"
#include "../physics/collision/CollisionChecker.h"

BoxCollider playerCollider(0, 0, 0, 0, 0, 0);
std::vector<BoxCollider> worldColliders;
CollisionChecker collisionChecker;
AbstractCamera* fpsCam;

int Game::init() {

    AbstractCamera* camera = new FreeCamera(glm::vec3(10.0f, 10.0f, 30.0f));

    DungeonGenerator dg(79, 24);
    glm::vec3 spawn = dg.generate(50);

    playerCollider = BoxCollider(glm::vec3(spawn.x - 0.1, spawn.y - 0.1, spawn.z - 0.1), glm::vec3(spawn.x + 0.1, spawn.y + 0.1, spawn.z + 0.1));
    std::cout << "min: " << playerCollider.minX << " " << playerCollider.minY << " " << playerCollider.minZ << " "
              << "max: " << playerCollider.maxX << " " << playerCollider.maxY << " " << playerCollider.maxZ << " " << std::endl;

    fpsCam = new FPSCamera(spawn);
    Input::setCamera(fpsCam);

    auto dungeon = dg.generateMesh(50);
    worldColliders = dungeon.second;
    for (auto c : worldColliders) {
        std::cout << "min: " << c.minX << " " << c.minY << " " << c.minZ << " "
                  << "max: " << c.maxX << " " << c.maxY << " " << c.maxZ << " " << std::endl;
    }

    auto ambientLight = glm::vec3(0.4f, 0.4f, 0.4f);
    shader = PhongShader("rendering/shading/shader_code/phongVertex.glsl", "rendering/shading/shader_code/phongFragment.glsl");

    Texture t0 = Texture("assets/textures/prototype-textures/PNG/Purple/texture_04.png");
    Texture t1 = Texture("assets/textures/prototype-textures/PNG/Dark/texture_13.png");

    dungeon.first[0].setMaterial(Material(t0, glm::vec3(.5f, .5f, .5f)));
    dungeon.first[1].setMaterial(Material(t1, glm::vec3(1.0f, 1.0f, 1.0f)));

    dungeon.first[0].init();
    dungeon.first[1].init();

    auto *dungeonFloor = new Actor();
    dungeonFloor->setMesh(dungeon.first[0]);
    auto *dungeonWalls = new Actor();
    dungeonWalls->setMesh(dungeon.first[1]);

    auto *playerCam = new Camera();
    playerCam->setCamera(fpsCam);
    playerCam->setIsActive(true);

    auto *light = new Light();
    light->setAmbientLight(ambientLight);
    light->setDirectionalLight(DirectionalLight());

    scene.setShader(shader);
    scene.addEntityToRoot(dungeonFloor);
    scene.addEntityToRoot(dungeonWalls);
    scene.addEntityToRoot(playerCam);
    scene.addEntityToRoot(light);

    return 0;
}

void Game::input() {
    if(Input::getKeyPressed(Window::window, GLFW_KEY_W)) Input::camera->ProcessKeyboard(FORWARD, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_S)) Input::camera->ProcessKeyboard(BACKWARD, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_A)) Input::camera->ProcessKeyboard(LEFT, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_D)) Input::camera->ProcessKeyboard(RIGHT, (float)Time::getDelta());
    if(Input::getKeyPressed(Window::window, GLFW_KEY_LEFT_SHIFT)) Input::camera->Sprint();
    if(Input::getKeyReleased(Window::window, GLFW_KEY_LEFT_SHIFT)) Input::camera->StopSprint();
}

void Game::update() {
    playerCollider.minX = fpsCam->position.x - 0.1;
    playerCollider.minY = fpsCam->position.y - 0.1;
    playerCollider.minZ = fpsCam->position.z - 0.1;
    playerCollider.maxX = fpsCam->position.x + 0.1;
    playerCollider.maxY = fpsCam->position.y + 0.1;
    playerCollider.maxZ = fpsCam->position.z + 0.1;
}

void Game::render() {
    scene.render();
    // check collision
    // TODO: only check colliders that are close to the player
    for (auto collider : worldColliders) {
        if (collisionChecker.collide(playerCollider, collider)) {
            std::cout << "Player is colliding with something" << std::endl;
        }
    }
}

void Game::dispose() {

}
