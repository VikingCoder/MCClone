//
// Created by Jonas on 6/26/23.
//

#ifndef MCCLONE_GAME_H
#define MCCLONE_GAME_H


#include "../rendering/shading/PhongShader.h"
#include "../rendering/scene-tree/Scene.h"

class Game {
public:
    int init();
    void input();
    void update();
    void render();
    void dispose();

private:
    PhongShader shader;
    Scene scene;
};


#endif //MCCLONE_GAME_H
