//
// Created by Jonas on 10/12/19.
//

#ifndef MCCLONE_DIRECTIONALLIGHT_H
#define MCCLONE_DIRECTIONALLIGHT_H


#include "BaseLight.h"

class DirectionalLight {
public:
    DirectionalLight() = default;

    BaseLight getBaseLight();
    void setBaseLight(BaseLight b);
    glm::vec3 getDirection();
    void setDirection(glm::vec3 d);

private:
    BaseLight base;
    glm::vec3 direction = glm::vec3(1.0f, 1.0f, 1.0f);
};


#endif //MCCLONE_DIRECTIONALLIGHT_H
