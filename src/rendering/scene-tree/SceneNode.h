//
// Created by Jonas on 5/29/23.
//

#ifndef MCCLONE_SCENENODE_H
#define MCCLONE_SCENENODE_H


#include "entities/Entity.h"

class SceneNode {
public:
    explicit SceneNode(Entity *entity);
    void addEntity(Entity *entityToAdd);
    Entity *getEntity();
    std::vector<SceneNode *> getChildren();
    ~SceneNode() {
        for (auto child : children) {
            delete child;
        }
    }
private:
    Entity *entity;
    std::vector<SceneNode *> children;
};


#endif //MCCLONE_SCENENODE_H
