//
// Created by Jonas on 6/29/23.
//

#ifndef MCCLONE_ACTOR_H
#define MCCLONE_ACTOR_H


#include "Entity.h"

class Actor : public Entity {
public:
    void setMesh(Mesh mesh);
    void rotate(float angle, glm::vec3 direction);
    void translate(float amount, glm::vec3 direction);
    void scale(float amount, glm::vec3 direction);
    void render(Shader *shader) override;
private:
    Mesh mesh;
};


#endif //MCCLONE_ACTOR_H
