//
// Created by Jonas on 6/30/23.
//

#ifndef MCCLONE_CAMERA_H
#define MCCLONE_CAMERA_H


#include "Entity.h"
#include "../../../game/CameraControllers/AbstractCamera.h"

class Camera : public Entity {
public:
    void setCamera(AbstractCamera *cam);
    void setIsActive(bool active);

    void render(Shader* shader) override;
private:
    bool isActive = false;
    AbstractCamera *camera = nullptr;
};


#endif //MCCLONE_CAMERA_H
