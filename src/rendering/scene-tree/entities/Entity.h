//
// Created by Jonas on 11/1/21.
//

#ifndef MCCLONE_ENTITY_H
#define MCCLONE_ENTITY_H


#include "../../Mesh.h"


class Entity {
public:
    Entity();
    explicit Entity(std::string name);
    uint64_t getId() const;
    std::string getName() const;

    virtual void render(Shader *shader);
private:
    uint64_t id;
    std::string name;


};


#endif //MCCLONE_ENTITY_H
