//
// Created by Jonas on 6/29/23.
//

#ifndef MCCLONE_LIGHT_H
#define MCCLONE_LIGHT_H


#include "Entity.h"
#include "../../lighting/DirectionalLight.h"

class Light : public Entity {
public:

    void setAmbientLight(glm::vec3 ambient);
    void setDirectionalLight(DirectionalLight directional);

    void render(Shader *shader) override;
private:
    // TODO: Make abstract Light and allow more different types of lighting to be rendered from scene tree
    glm::vec3 ambientLight{};
    DirectionalLight light{};
};


#endif //MCCLONE_LIGHT_H
