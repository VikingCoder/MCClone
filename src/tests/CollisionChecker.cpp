//
// Created by Jonas on 11/25/23.
//

#include <gtest/gtest.h>
#include "../physics/collision/CollisionChecker.h"

class CollisionCheckerTest : public testing::Test {
};

TEST(CollisionCheckerTest, BoxBox) {
    CollisionChecker c;

    BoxCollider a(0, 1, 0, 1, 0, 1);
    BoxCollider b(0, 1, 0, 1, 0, 1);

    EXPECT_TRUE(c.collide(a, b));

    b = BoxCollider(1, 2, 1, 2, 1, 2);

    EXPECT_TRUE(c.collide(a, b));

    b = BoxCollider(1.1, 2, 1.1, 2, 1.1, 2);

    EXPECT_FALSE(c.collide(a, b));
}

TEST(CollisionCheckerTest, BoxPoint) {
    CollisionChecker c;

    BoxCollider a(0, 1, 0, 1, 0, 1);
    PointCollider b(0, 0, 0);

    EXPECT_TRUE(c.collide(a, b));

    b = PointCollider(0.5, 0.5, 2);

    EXPECT_FALSE(c.collide(a, b));
}

TEST(CollisionCheckerTest, BoxSphere) {
    CollisionChecker c;

    BoxCollider a(0, 1, 0, 1, 0, 1);
    SphereCollider b(0, 0, 0, 1);

    EXPECT_TRUE(c.collide(a, b));

    b = SphereCollider(2, 0, 0, 1.1);

    EXPECT_TRUE(c.collide(a, b));

    b = SphereCollider(2, 0, 0, 0.99);

    EXPECT_FALSE(c.collide(a, b));
}

TEST(CollisionCheckerTest, PointPoint) {
    CollisionChecker c;

    PointCollider a(0, 0, 0);
    PointCollider b(0, 0, 0);

    EXPECT_TRUE(c.collide(a, b));

    b = PointCollider(0, 0, 0.1);

    EXPECT_FALSE(c.collide(a, b));
}

TEST(CollisionCheckerTest, PointSphere) {
    CollisionChecker c;

    PointCollider a(0, 0, 0);
    SphereCollider b(1, 0, 0, 1.01);

    EXPECT_TRUE(c.collide(a, b));

    b = SphereCollider(1, 0, 0, 0.2);

    EXPECT_FALSE(c.collide(a, b));
}

TEST(CollisionCheckerTest, SphereSphere) {
    CollisionChecker c;

    SphereCollider a(0, 0, 0, 0.51);
    SphereCollider b(0, 0, 1, 0.5);

    EXPECT_TRUE(c.collide(a, b));

    b = SphereCollider(0, 0, 1, 0.4);

    EXPECT_FALSE(c.collide(a, b));
}