//
// Created by Jonas on 10/9/19.
//

#ifndef MCCLONE_MODELLOADER_H
#define MCCLONE_MODELLOADER_H


#include "../rendering/Mesh.h"

class ModelLoader {
public:
    static Mesh loadMesh(const std::string& fileName);
private:
    static std::vector<std::string> split(std::string s, const std::string &delimiter);
};


#endif //MCCLONE_MODELLOADER_H
